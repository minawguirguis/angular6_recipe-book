import { Component } from '@angular/core';
import { Response } from '@angular/http';

import {DataStorageService} from '../../shared/data-storage.service';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  constructor(private dataStorageServices: DataStorageService,
              private authService: AuthService) {}

  onSaveData() {
    this.dataStorageServices.storeRecipes()
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      );
  }

  onFetchRecipe() {
    this.dataStorageServices.getRecipes();
  }
  onLogOut() {
    this.authService.logout();
  }
  isAuthenticated() {
    return this.authService.isAuthenticated();
  }
}
